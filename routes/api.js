const express = require('express');
const router = express.Router();
const Item = require('../models/item');

// Get a list of examples from the database
router.get('/examples', function(req, res, next){
    Item.aggregate([
        {
            $geoNear: {
                near: {type:'Point', coordinates:[parseFloat(req.query.lng), parseFloat(req.query.lat)]},
                distanceField: "dist.calculated",
                maxDistance: 100000,
                spherical: true                
            }
        }
    ]).then(function(items){
        res.send(items)
    })
});

// Add a new item to the database
router.post('/examples', function(req, res, next){
    Item.create(req.body).then(function(item){
        res.send(item);  
    }).catch(next);
});

// Update item in the database
router.put('/examples/:id', function(req, res, next){
    Item.findOneAndUpdate({_id: req.params.id}, req.body).then(function(item){
        res.send(item);
    })
});

// Delete item from the database
router.delete('/examples/:id', function(req, res, next){
    Item.findOneAndDelete({_id: req.params.id}).then(function(item){
        res.send(item);
    })
});

module.exports = router;
