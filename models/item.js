const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create geolocation Schema
const GeoSchema = new Schema({
    type:{
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        index: "2dsphere"
    }
});

// create item Schema & Model
const ItemSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Name field is required']
    },
    class: {
        type: String,
        required: [true, 'Class field is required']
    },
    grade: {
        type: String
    },
    // add in geo location
    geometry: GeoSchema
});

const Item = mongoose.model('item', ItemSchema);
module.exports = Item;
