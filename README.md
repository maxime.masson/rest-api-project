# REST API Tutorial

`Channel` : [The Net Ninja](https://www.youtube.com/channel/UCW5YeuERMmlnqo4oq8vwUpg)

`Repo`: [reste-api-playlist](https://github.com/iamshaunjp/rest-api-playlist)

- Learn about HHTP methods and what they're for
- Create an API using Node.js, Express & MongoDB
- Test the API using `Postman`
- Create a simple front-end to interact with the API

## API

Application Programming Interface.

Some popular examples : 
- Youtube API
- Google Map API
- Twitter API
- Uber API



## REST

Representaitional State Transfer

- Architectual style of the web
- A standard / set of guidelines by which we can structure & create API's
- REST API's have identifiable properties

### PROPERTIES

**They make use of reasource-based URL's**

Let's say we send a get request to a webpage (hhtp://www.footbal.com/scores).

The API will return a JSON file from when get resquest (http://www.football.com/api/scores).

```json
"scores": {
    "team" : "arsenal",
    "for" : 10,
    "against" : 50,
    "wins" : 1,
    "losses" : 37,
    "draws" : 0
}
```

**They make use of HTTP Methods**

- GET (Used to retrieve data from the server)
- POST (Used to send data to the server)
- PUT (Used to  update data)
- DELETE (Used to delete data)

**They make use of HTTP Status code**

- 200 means OK
- 404 means resource not found
- 500 means server error


## THE SETUP

- MongoDB
- Node.JS express app
- Front-end
- Mobile App
- Other Website

### The data for the API

We're using the HTTP request. The mongodb database will communicate with the front-end app using the request.
- Create a new item (POST request)
- Reading item (GET request)
- Updating / Editing item (PUT request)
- Deleting item (DELETE request).

The `Express/Node.js` app (server) will be the medium between the front end / MondoBD database.

### The routes

To communicate with the API we have to make request to certain *routes / end point* that the API expose to us.

Ex: `http://www.website.com/api/example` -> *Note the '/api' route*.

```js
$.ajax({
    method:"POST",
    url:"http://www.website.com/api/examples",
    data: { name: "Example", rank: "Pro" }
})

$.ajax({
    method:"GET",
    url:"http://www.website.com/api/examples"
})
```

***Exemples above shows how the server understand how to differentiate the requests***.

## Express

Package which make life easier. Everything we can do with Express we can do without.

How to install : 
1. npm install express --save (this will save the dependancies in the json file)
2. Create the index.js file (as the main entry file in json file).

**Setup Express**
```js
const express = require('express');

// set up express app
const app = express();

// listen for request
// process port : If external app has env variable setup for a port
app.listen(process.env.port || 4000, function() {
    console.log('Listening for requests');
});
```

### Setup Route with Express

1. Create a directory named `routes`.
2. Create `api.js` inside the directory.
3. Instantiate `const router = express.Router()` into the API file.
4. Add each request following the following format : 
    ```js
    router.get('/examples', function(req, res){
    res.send({type: 'GET'});
    });
    ```
5. Export de router using `module.exports = router`.
6. Import de router into the `index.js` file using `const routes = require('./routes/api')`.
6. Use the router with a specific path (Such as '/api') -> `app.use('/api', routes)`.

**My API file**

```js 
const express = require('express');
const router = express.Router();

// Get a list of examples from the database
router.get('/examples', function(req, res){
    res.send({type: 'GET'});
});

// Add a new item to the database
router.post('/examples', function(req, res){
    res.send({type: 'POST'});
});

// Update item in the database
router.put('/examples/:id', function(req, res){
    res.send({type: 'PUT'});
});

// Delete item from the database
router.delete('/examples/:id', function(req, res){
    res.send({type: 'DELETE'});
});

module.exports = router;
```

### Postman

Used to test out all the routes in the application.


## MongoDB

Database

### Models

Represents collections in MongoDB
- User Model to represent a collection of Users
- Item Model to represent a collection of items

### Schemas

Define the structure of an object within a collection 

***Example***
```json
{
    name: String,
    state: String,
    available: boolean
}
```

### Mongoose

Help interact with the database. The plugins add a layer of methods to easily save, edit, retreive and delete data from `MongoDB`.
- Also allows to create our Models and Schemas easily

**To install**
1. `npm instal mongoose --save`

**Create new Models**

1. Create directory at the root `models`.
2. Create a js file with the name of the required models (ex : item.js)
3. Add following line at the beginning : 
    ```js
    const mongoose = require('mongoose');
    const Schema = mongoose.Schema;

    // create item Schema & Model
    const ItemSchema = new Schema({
        name: {
            type: String,
            required: [true, 'Name field is required']
        },
        type: {
            type: String,
            required: [true, 'Type field is required']
        },
        available: {
            type: Boolean,
            default: false
        }
        // add in geo location
    });

    const Item = mongoose.model('item', ItemSchema);
    module.exports = Item;
    ```

**Connect to MongoDB**

Simply add the following line to the `index.js` file. 
-> mongoose.Promise is writting to override the mongoose Promise as it's deprecated.

```js
// connect to mongoDB
mongoose.connect('mongodb://localhost:uqam', { useNewUrlParser: true, useFindAndModify: false });
mongoose.Promise = global.Promise;
```

***UPDATE*** : Lot of drivers are deprecated. If error while using MongoDB/Mongoose, look at the error and add the required object params to the `connect()` method such as `{ useNewUrlParser: true, useFindAndModify: false }`.


**Save data to the DataBase**

First, make sure your schema(s) are imported into your `api.js` file.

```js
const Item = require('../models/item');
```

Then, in the `POST` method, add the save() methods.

```js
var item = new Item(req.body);
item.save();
// OR
Item.create(req.body);
```

### RoboMongo

Interface to take a look at local mongo database.

***To download*** : [Robot 3T](https://robomongo.org/download)

### Error Handling

If we try to make a `POST` request with missing *required* information in the object, an error will be log to the console.
Now, how to handle the error ?

1. Create middleware `next`. 
2. `Catch` the err in your request
    ```js
    router.post('/examples', function(req, res, next){
        Item.create(req.body).then(function(item){
            res.send(item);  
        }).catch(next);
    });
    ```
3. Add middleware to your `index.js`file
    ```js
    // Error handling middleware
    // Can take up to 4 parameters
    app.use(function(err, req, res, next){
        //console.log(err);
        res.status(422).send({error: err.message});
    });
    ```

### Delete an Item from the DataBase

We'll be using the methods `findByIdAndRemove()` given by `Mangoose`.

```js
// Delete item from the database
router.delete('/examples/:id', function(req, res, next){
    Item.findOneAndDelete({_id: req.params.id}).then(function(item){
        res.send(item);
    })
});
```

### Update an Item from the Database

We'll be using the method `findByIdAndUpdate()` given by `Mangoose`.

This method is looking for the ID and is updating the body. Then, with the `then()` method, we can send back the item to the user.
```js
// Update item in the database
router.put('/examples/:id', function(req, res, next){
    Item.findOneAndUpdate({_id: req.params.id}, req.body).then(function(item){
        res.send(item);
    })
});
```

### Handle GET Request

URL Params : Can add on parameters in request URL's

**Ex :** www.website.com/api/examples?lng=50.45&lat=42.35


## GeoJson

Code to use for geoNear

```js
// Get a list of examples from the database
router.get('/examples', function(req, res, next){
    Item.aggregate([
        {
            $geoNear: {
                near: {type:'Point', coordinates:[parseFloat(req.query.lng), parseFloat(req.query.lat)]},
                distanceField: "dist.calculated",
                maxDistance: 100000,
                spherical: true                
            }
        }
    ]).then(function(items){
        res.send(items)
    })
});
```

## Front-End with React

First, we will have to add another `Middleware` to your **index.js** file. Why ? We have to handle static file.

To do so, we're using `express.static()`.

### Library

We could import WebPack.

OR

Download
- React-DOM
- React
- Babel

### React Component

Create a react component that react with our API.


